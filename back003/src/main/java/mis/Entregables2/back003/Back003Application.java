package mis.Entregables2.back003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Back003Application {

	public static void main(String[] args) {
		SpringApplication.run(Back003Application.class, args);
	}

}
