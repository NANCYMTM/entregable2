package mis.Entregables2.back003.controlador;

import mis.Entregables2.back003.modelo.ProductModel;
import mis.Entregables2.back003.servicios.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
@RestController
@RequestMapping("/produc2/v1")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductModel> getProductos(){
        return productoService.findAll();
    }

    @GetMapping("/productos/{idProducto}")
    public Optional<ProductModel> getProductoId(@PathVariable String idProducto){
        return productoService.findById(idProducto);
    }
    @PostMapping("/productos")
    public ProductModel postProductos(@RequestBody ProductModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }
    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductModel productoToUpdate){
        productoService.save(productoToUpdate);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductModel productoToDelete){
        return productoService.delete(productoToDelete);
    }
}











