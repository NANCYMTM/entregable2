package mis.Entregables2.back003.modelo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ProductModel {

        @Id @JsonProperty(access= JsonProperty.Access.READ_WRITE)
        private String idProducto;
        private String descProducto;
        private Double precProducto;

        public ProductModel() {

        }

        public ProductModel(String idProducto, String descProducto, Double precProducto) {
            this.idProducto = idProducto;
            this.descProducto = descProducto;
            this.precProducto = precProducto;
        }


        public String getIdProducto() {
            return idProducto;
        }

        public void setIdProducto(String idProducto) {
            this.idProducto = idProducto;
        }

        public String getDescProducto() {
            return descProducto;
        }

        public void setDescProducto(String descProducto) {
            this.descProducto = descProducto;
        }

        public Double getPrecProducto() {
            return precProducto;
        }

        public void setPrecProducto(Double precProducto) {
            this.precProducto = precProducto;
        }

}
