package mis.Entregables2.back003.servicios;

import mis.Entregables2.back003.modelo.ProductModel;
import mis.Entregables2.back003.servicios.repositorios.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    public List<ProductModel> findAll(){
        return productoRepository.findAll();
    }

    public Optional<ProductModel> findById(String idProducto){
        return productoRepository.findById(idProducto);
    }

    public ProductModel save(ProductModel entity){
        return productoRepository.save(entity);
    }

    public boolean delete(ProductModel entity){
        try {
            productoRepository.delete(entity);
            return true;
        } catch(Exception ex) {
             return false;
        }
    }
}
