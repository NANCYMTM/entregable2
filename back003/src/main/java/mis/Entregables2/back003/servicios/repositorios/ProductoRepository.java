package mis.Entregables2.back003.servicios.repositorios;

import mis.Entregables2.back003.modelo.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductModel,String> {
}
